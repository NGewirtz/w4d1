class CommentsController < ApplicationController

  def index

    if params[:artwork_id]
      artwork = Artwork.find(params[:artwork_id])
      comments = artwork.comments
    elsif params[:user_id]
      user = User.find(params[:user_id])
      comments = user.comments
    end
    render json: comments
  end


  def comment_artwork_params
    params.require(:comment).permit(:artwork_id)
  end

  def comment_user_params
    params.require(:comment).permit(:user_id)
  end

end
