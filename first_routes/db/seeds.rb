# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)

User.destroy_all
john = User.create(username: 'johndoe')
elliott = User.create(username: 'elliott')
neil = User.create(username: 'neil')
chao = User.create(username: 'chao')
jane = User.create(username: 'janedoe')


Artwork.destroy_all
a1 = Artwork.create(title: 'Sunny Day', image_url: 'abc123', artist_id: john.id)
a2 = Artwork.create(title: 'Cloudy Day', image_url: 'def123', artist_id: john.id)
a3 = Artwork.create(title: 'Rainy Day', image_url: 'test1234', artist_id: john.id)
a4 = Artwork.create(title: 'Bowling Alley', image_url: 'bowlbowl', artist_id: elliott.id)
a5 = Artwork.create(title: 'Roller Rink', image_url: 'rinkskaterink', artist_id: elliott.id)
a6 = Artwork.create(title: 'App Academy', image_url: 'tiredtired', artist_id: neil.id)
a7 = Artwork.create(title: 'Green Shirt', image_url: 'greensleeves', artist_id: chao.id)
a8 = Artwork.create(title: 'Upper West Side', image_url: 'subwayaccess', artist_id: jane.id)
a9 = Artwork.create(title: 'Flourescent Lights', image_url: 'depressinglights123', artist_id: jane.id)

ArtworkShare.destroy_all
as1 = ArtworkShare.create(viewer_id: john.id, artwork_id: a9.id)
as2 = ArtworkShare.create(viewer_id: john.id, artwork_id: a8.id)
as3 = ArtworkShare.create(viewer_id: elliott.id, artwork_id: a8.id)
as4 = ArtworkShare.create(viewer_id: neil.id, artwork_id: a8.id)
as5 = ArtworkShare.create(viewer_id: neil.id, artwork_id: a9.id)
as6 = ArtworkShare.create(viewer_id: neil.id, artwork_id: a7.id)
as7 = ArtworkShare.create(viewer_id: jane.id, artwork_id: a1.id)
as8 = ArtworkShare.create(viewer_id: jane.id, artwork_id: a2.id)
as9 = ArtworkShare.create(viewer_id: chao.id, artwork_id: a2.id)
as10 = ArtworkShare.create(viewer_id: chao.id, artwork_id: a3.id)

Comment.destroy_all
a = Comment.create(user_id: john.id, artwork_id: a1.id, body: "My work is great!!!!!")
b = Comment.create(user_id: john.id, artwork_id: a2.id, body: "2nd co,,ent")
c = Comment.create(user_id: neil.id, artwork_id: a3.id, body: "3rd comment")
d = Comment.create(user_id: neil.id, artwork_id: a4.id, body: "new thing")
e = Comment.create(user_id: chao.id, artwork_id: a5.id, body: "hello world")
f = Comment.create(user_id: elliott.id, artwork_id: a6.id, body: "My work is great tooooo!!!!!")
